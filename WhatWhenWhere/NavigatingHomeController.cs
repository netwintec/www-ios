using Foundation;
using System;
using UIKit;
using ObjCRuntime;

namespace WhatWhenWhere
{
    public partial class NavigatingHomeController : UINavigationController
    {
        public NavigatingHomeController (IntPtr handle) : base (handle)
        {
        }

        public override void DismissViewController(bool animated, [BlockProxy(typeof(Action))] Action completionHandler)
        {
            if(this.PresentedViewController != null)
                base.DismissViewController(animated, completionHandler);
        }

        public override void DismissModalViewController(bool animated)
        {
            if (this.PresentedViewController != null)
                base.DismissModalViewController(animated);
        }
    }
}