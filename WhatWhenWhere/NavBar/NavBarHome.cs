using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace WhatWhenWhere
{
	partial class NavBarHome : UINavigationBar
	{

        UIImageView back,centerlogo, Menu, Not,Rel;
        bool visible = false;
        public static NavBarHome Instance { private set; get; }

        public NavBarHome (IntPtr handle) : base (handle)
		{
            NavBarHome.Instance = this;
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            try
            {
                back.RemoveFromSuperview();
                centerlogo.RemoveFromSuperview();
                Menu.RemoveFromSuperview();
                Not.RemoveFromSuperview();
            }
            catch (Exception e) { }


            //Console.WriteLine(Frame.Width);

            centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 34, 9.5, 68, 25));
            centerlogo.Image = UIImage.FromFile("header.png");

            back = new UIImageView(new CGRect(10, 12, 21, 20));
            if(visible)
                back.Image = UIImage.FromFile("alpha.png");
            else
                back.Image = UIImage.FromFile("alpha.png");

            Menu = new UIImageView(new CGRect(this.Frame.Width - 30, 12, 20, 20));
            Menu.Image = UIImage.FromFile("settings.png");

            Not = new UIImageView(new CGRect(this.Frame.Width - 70, 12, 20, 20));
            Not.Image = UIImage.FromFile("notifica.png");

            Rel = new UIImageView(new CGRect(41, 12, 20, 20));
            Rel.Image = UIImage.FromFile("reload.png");

            AddSubview(centerlogo);
            AddSubview(Menu);
            AddSubview(back);
            AddSubview(Not);
            AddSubview(Rel);

        }

        public void AddBack()
        {
            back.Image = UIImage.FromFile("back_icon.png");
            visible = true;
        }

        public void RemoveBack()
        {
            back.Image = UIImage.FromFile("alpha.png");
            visible = false;
        }
    }
}
