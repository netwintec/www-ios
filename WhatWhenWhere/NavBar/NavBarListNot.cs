using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace WhatWhenWhere
{
    public partial class NavBarListNot : UINavigationBar
    {

        UIImageView back, centerlogo, Menu;

        public NavBarListNot (IntPtr handle) : base (handle)
        {
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            try
            {
                back.RemoveFromSuperview();
                centerlogo.RemoveFromSuperview();
                Menu.RemoveFromSuperview();
            }
            catch (Exception e) { }

            //Console.WriteLine(Frame.Width);

            centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 34, 9.5, 68, 25));
            centerlogo.Image = UIImage.FromFile("header.png");

            back = new UIImageView(new CGRect(10, 12, 21, 20));
            back.Image = UIImage.FromFile("back_icon.png");

            Menu = new UIImageView(new CGRect(this.Frame.Width - 30, 12, 20, 20));
            Menu.Image = UIImage.FromFile("settings.png");

            AddSubview(centerlogo);
            AddSubview(Menu);
            AddSubview(back);

        }
    }
}