using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace WhatWhenWhere
{
    public partial class NavBarSettings : UINavigationBar
    {

        UIImageView back, centerlogo, Not;

        public NavBarSettings (IntPtr handle) : base (handle)
        {
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            try
            {
                back.RemoveFromSuperview();
                centerlogo.RemoveFromSuperview();
                Not.RemoveFromSuperview();
            }
            catch (Exception e) { }

            //Console.WriteLine(Frame.Width);

            centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 34, 9.5, 68, 25));
            centerlogo.Image = UIImage.FromFile("header.png");

            back = new UIImageView(new CGRect(10, 12, 21, 20));
            back.Image = UIImage.FromFile("back_icon.png");

            Not = new UIImageView(new CGRect(this.Frame.Width - 60, 12, 20, 20));
            Not.Image = UIImage.FromFile("notifica.png");

            AddSubview(centerlogo);
            AddSubview(back);
            AddSubview(Not);

        }

    }
}