﻿using System;
using System.Collections.Generic;
using System.Text;
using Foundation;
using UIKit;
using RestSharp;

namespace WhatWhenWhere
{
    partial class TableSourceNot : UITableViewSource
    {

        public List<ListNot> listNot;
        public int element;
        int selected;


        public TableSourceNot(List<ListNot> list)
        {
            listNot = list;
            element = listNot.Count;
            //element = e;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return element;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            ListNotifichePage.Instance.Selected = indexPath.Row;
            ListNotifichePage.Instance.SelectedNot(indexPath.Row);

        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            nfloat height = 80;
            return height;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {



            int row = indexPath.Row;
            NSString cellIdentifier = new NSString(row.ToString());

            CustomCellNot cell = new CustomCellNot(cellIdentifier,listNot[row].Nome, listNot[row].Category, listNot[row]);
            cell.UpdateCell();



            return cell;

        }

        

    }
}

