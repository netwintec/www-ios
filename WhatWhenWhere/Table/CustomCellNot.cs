﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UIKit;

namespace WhatWhenWhere
{
    class CustomCellNot : UITableViewCell
    {
        UILabel nomeLabel,categoryLabel;
        UIView separator;
        UIImageView image;

        CGSize sizeN, sizeC;

        public ListNot obj;
        public CustomCellNot(NSString cellId,string n,string c,ListNot o) : base(UITableViewCellStyle.Default, cellId)
        {

            obj = o;
            BackgroundColor = UIColor.Black;

            separator = new UIView();
            separator.BackgroundColor = UIColor.FromRGB(255,204,0);

            sizeN = UIStringDrawing.StringSize(n, UIFont.FromName("Lato-Regular", 16), new CGSize(ContentView.Frame.Width - 140, 2000));
            sizeC = UIStringDrawing.StringSize(c, UIFont.FromName("Lato-Regular", 14), new CGSize(ContentView.Frame.Width - 140, 2000));

            nomeLabel = new UILabel()
            {
                Font = UIFont.FromName("Lato-Regular", 16f),
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear,
                Lines = 0
            };

            categoryLabel = new UILabel()
            {
                Font = UIFont.FromName("Lato-Regular", 14f),
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear

            };

            image = new UIImageView();

            ContentView.AddSubviews(new UIView[] { image,nomeLabel,categoryLabel,separator });

        }
        public void UpdateCell()
        {

            nomeLabel.Text = obj.Nome;
            categoryLabel.Text = obj.Category;
            image.Image = UIImage.FromFile("header.png");
            if (obj.UIImg == null)
                SetImageAsync(image, obj.Img, obj);
            else
                image.Image = obj.UIImg;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            float totalH = (float)(sizeN.Height + sizeC.Height + 5);

            if(obj.UIImg == null)
                image.Frame = new CGRect(30, 29, 60, 22);
            else
            {

                nfloat w = obj.UIImg.Size.Width;
                nfloat h = obj.UIImg.Size.Height;

                if (w > h)
                {

                    float newWidht = (float)60;
                    float newHeight = (float)((60 * h) / w);

                    image.Frame = new CGRect(30, 40 - (newHeight / 2), newWidht, newHeight);

                }
                else
                {

                    float newHeight = (float)60;
                    float newWidht = (float)((60 * w) / h);

                    image.Frame = new CGRect(60 - (newWidht / 2), 10, newWidht, newHeight);

                }

            }
            nomeLabel.Frame = new CGRect(110, 40 - (totalH / 2) , widht - 140, sizeN.Height);
            categoryLabel.Frame = new CGRect(110, 40 - (totalH / 2) + (sizeN.Height + 5), widht - 140, sizeC.Height);

            separator.Frame = new CGRect(30, ContentView.Frame.Height - 1, ContentView.Frame.Width - 60, 1);
        }

        public void SetImageAsync(UIImageView image, string url, ListNot obj)
        {
            UIImage Placeholder = UIImage.FromFile("header.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;

                    InvokeOnMainThread(() =>
                    {
                        Console.WriteLine(url+"|"+(cachedImage == Placeholder));

                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        if (w > h) { 

                            float newWidht = (float)60;
                            float newHeight = (float)((60 * h) / w);

                            image.Frame = new CGRect(30, 40 - (newHeight / 2), newWidht, newHeight);

                        }else
                        {

                            float newHeight = (float)60;
                            float newWidht = (float)((60 * w) / h);

                            image.Frame = new CGRect(60 - (newWidht / 2), 10, newWidht, newHeight);

                        }

                        image.Image = cachedImage;
                        obj.UIImg = cachedImage;
                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }


        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            Console.WriteLine("Error Load Image");
            return null;
        }

    }
}
