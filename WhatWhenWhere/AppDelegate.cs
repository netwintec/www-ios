﻿using Foundation;
using UIKit;
using System;
using RestSharp;
using System.Threading.Tasks;

namespace WhatWhenWhere
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        bool SportNot, TorneiNot, RaduniNot, MostreNot, SfilateNot;
        bool VitaNot, AperitiviNot, GayNot, OtakuNot, ConcertiNot;
        bool MercatiniNot, FiereNot, SagreNot, InaugurazioniNot, CinemaNot;
        bool TeatroNot, OnlusNot, ReligioneNot, BambiniNot, ItinerariNot;
        bool CorsiNot, PromoNot, ScioperiNot, PropagandaNot;

        public string APNToken="";
        public bool notification=false;

        public bool isHome, isSetting, isList, isNotification;

        public override UIWindow Window
        {
            get;
            set;
        }

        public static AppDelegate Instance { private set; get; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method
            AppDelegate.Instance = this;

            isHome = false;
            isSetting = false;
            isList = false;
            isNotification = false;

            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

            //NOTIFICATION
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            if (launchOptions != null && launchOptions.Keys != null && launchOptions.Keys.Length != 0 && launchOptions.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                Console.WriteLine("entro");
                NSDictionary userInfo = launchOptions.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                NSDictionary userInfo2;
                string url = String.Empty;

                foreach (var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("data")))
                {
                    userInfo2 = (userInfo[new NSString("data")] as NSDictionary);
                    if (userInfo2.ContainsKey(new NSString("url")))
                        url = (userInfo2[new NSString("url")] as NSString).ToString();

                }

                Console.WriteLine("URL-->" + url);

                //url = "http://www.whatwhenwhere.it/?places";

                NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                notification = true;

                Console.WriteLine("PUSH:" + NSUserDefaults.StandardUserDefaults.StringForKey("NotificationUrlWWW"));
                //ProcessNotification(UIApplicationLaunchOptionsRemoteNotificationKey);
            }

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.

            Console.WriteLine("App moving to inactive state.");
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
            Console.WriteLine("App entering background state.");
            Console.WriteLine("Now receiving location updates in the background");
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
            Console.WriteLine("App will enter foreground");
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
            Console.WriteLine("App is becoming active");
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {

            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            APNToken = DeviceToken;
            Console.WriteLine(APNToken);

            if(ViewController.Instance.prima == null || ViewController.Instance.prima == false)
            {

                ViewController.Instance.InvokeOnMainThread(() => {

                    Console.WriteLine("Notification Send at http://api.whatwhenwhere.it/devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position");
                    var client = new RestClient("http://api.whatwhenwhere.it/");

                    var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/categories", Method.POST);
                    //request.AddHeader("Content-Type", "multipart/form-data");

                    //JObject oJsonObject = new JObject();


                    //oJsonObject.Add("os", "Android");
                    //oJsonObject.Add("latitude", location.Latitude);
                    //oJsonObject.Add("longitude", location.Longitude);

                    request.AddParameter("os", "iOS");
                    request.AddParameter("token", APNToken);

                    request.AddParameter("categories[]", "sport");
                    request.AddParameter("categories[]", "tournaments");
                    request.AddParameter("categories[]", "gatherings");
                    request.AddParameter("categories[]", "exhibitions");
                    request.AddParameter("categories[]", "parades");
                    request.AddParameter("categories[]", "nightlife");
                    request.AddParameter("categories[]", "aperitives");
                    request.AddParameter("categories[]", "gay-lgbt");
                    request.AddParameter("categories[]", "otaku");
                    request.AddParameter("categories[]", "concerts");
                    request.AddParameter("categories[]", "markets");
                    request.AddParameter("categories[]", "fairs");
                    request.AddParameter("categories[]", "festivals");
                    request.AddParameter("categories[]", "inaugurations");
                    request.AddParameter("categories[]", "cinema");
                    request.AddParameter("categories[]", "theatre");
                    request.AddParameter("categories[]", "onlus");
                    request.AddParameter("categories[]", "religion");
                    request.AddParameter("categories[]", "kids");
                    request.AddParameter("categories[]", "routes");
                    request.AddParameter("categories[]", "training-courses");
                    request.AddParameter("categories[]", "promos-discounts");
                    request.AddParameter("categories[]", "strike");
                    request.AddParameter("categories[]", "propaganda");

                    //requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                    foreach (var p in request.Parameters)
                    {
                        Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
                    }

                    client.ExecuteAsync(request, (s, e) =>
                    {

                        Console.WriteLine("Result Not:" + s.StatusCode + "|" + s.Content);

                    });

                    Console.WriteLine(1);

                });


            }
            else
            {

                SportNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSport");
                TorneiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWTornei");
                RaduniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWRaduni");
                MostreNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWMostre");
                SfilateNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSfilate");
                VitaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWVita");
                AperitiviNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWAperitivi");
                GayNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWGay");
                OtakuNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWOtaku");
                ConcertiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWConcerti");
                MercatiniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWMercatini");
                FiereNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWFiere");
                SagreNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSagre");
                InaugurazioniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWInaugurazione");
                CinemaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWCinema");
                TeatroNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWTeatro");
                OnlusNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWOnlus");
                ReligioneNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWReligione");
                BambiniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWBambini");
                ItinerariNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWItinerari");
                CorsiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWCorsi");
                PromoNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWPromo");
                ScioperiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWScioperi");
                PropagandaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWPropaganda");

                Console.WriteLine("Notification Send at http://api.whatwhenwhere.it/devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position");
                
                //******* PRODUZIONE *****//
                var client = new RestClient("http://api.whatwhenwhere.it/");
                var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/categories", Method.POST);

                //******* TEST ********//
                //var client = new RestClient("http://test.keepup.pro/");
                //var request = new RestRequest("provauser.php", Method.POST); 

                //request.AddHeader("Content-Type", "multipart/form-data");

                request.AddParameter("os", "iOS");
                request.AddParameter("token", APNToken);

                if (SportNot)
                    request.AddParameter("categories[]", "sport");
                if (TorneiNot)
                    request.AddParameter("categories[]", "tournaments");
                if (RaduniNot)
                    request.AddParameter("categories[]", "gatherings");
                if (MostreNot)
                    request.AddParameter("categories[]", "exhibitions");
                if (SfilateNot)
                    request.AddParameter("categories[]", "parades");
                if (VitaNot)
                    request.AddParameter("categories[]", "nightlife");
                if (AperitiviNot)
                    request.AddParameter("categories[]", "aperitives");
                if (GayNot)
                    request.AddParameter("categories[]", "gay-lgbt");
                if (OtakuNot)
                    request.AddParameter("categories[]", "otaku");
                if (ConcertiNot)
                    request.AddParameter("categories[]", "concerts");
                if (MercatiniNot)
                    request.AddParameter("categories[]", "markets");
                if (FiereNot)
                    request.AddParameter("categories[]", "fairs");
                if (SagreNot)
                    request.AddParameter("categories[]", "festivals");
                if (InaugurazioniNot)
                    request.AddParameter("categories[]", "inaugurations");
                if (CinemaNot)
                    request.AddParameter("categories[]", "cinema");
                if (TeatroNot)
                    request.AddParameter("categories[]", "theatre");
                if (OnlusNot)
                    request.AddParameter("categories[]", "onlus");
                if (ReligioneNot)
                    request.AddParameter("categories[]", "religion");
                if (BambiniNot)
                    request.AddParameter("categories[]", "kids");
                if (ItinerariNot)
                    request.AddParameter("categories[]", "routes");
                if (CorsiNot)
                    request.AddParameter("categories[]", "training-courses");
                if (PromoNot)
                    request.AddParameter("categories[]", "promos-discounts");
                if (ScioperiNot)
                    request.AddParameter("categories[]", "strike");
                if (PropagandaNot)
                    request.AddParameter("categories[]", "propaganda");

                //requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                foreach (var p in request.Parameters)
                {
                    Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
                }

                client.ExecuteAsync(request, (s, e) =>
                {

                    Console.WriteLine("Result Not:" + s.StatusCode + "|" + s.Content);

                });

                Console.WriteLine(1);


            }

            //var client = new RestClient("http://api.whatwhenwhere.it/");

            //var requestN4U = new RestRequest("devices/"++"/categories", Method.POST);


            //client.ExecuteAsync(requestN4U, null);


        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }


        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.

            Console.WriteLine("remote" + application.ApplicationState.ToString());

            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {


                Console.WriteLine("back");

                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                NSDictionary userInfo2;
                string url = String.Empty;

                foreach (var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("data")))
                {
                    userInfo2 = (userInfo[new NSString("data")] as NSDictionary);
                    if (userInfo2.ContainsKey(new NSString("url")))
                        url = (userInfo2[new NSString("url")] as NSString).ToString();

                }

                Console.WriteLine("URL-->" + url);

                NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");


                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("Main_iPad", null);
                }

                UIViewController lp = storyboard.InstantiateViewController("Notification");
                //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                TopController().PresentModalViewController(lp, true);

            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                NSDictionary userInfo2;
                string url = String.Empty;
                string name = String.Empty;
                foreach (var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("data")))
                {
                    userInfo2 = (userInfo[new NSString("data")] as NSDictionary);
                    if (userInfo2.ContainsKey(new NSString("url")))
                        url = (userInfo2[new NSString("url")] as NSString).ToString();
                    if (userInfo2.ContainsKey(new NSString("name")))
                        name = (userInfo2[new NSString("name")] as NSString).ToString();
                }

                Console.WriteLine("URL-->" + url );

                //url = "http://www.whatwhenwhere.it/?places";

                if(isHome)
                    HomePageController.Instance.Notification(url, name);
                if (isSetting)
                    SettingPage.Instance.Notification(url, name);
                if (isList)
                    ListNotifichePage.Instance.Notification(url, name);
                if (isNotification)
                    NotificationController.Instance.Notification(url, name);

                /*NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("Main_iPad", null);
                }

                UIViewController lp = storyboard.InstantiateViewController("Notification");
                //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                TopController().PresentModalViewController(lp, true);*/

            }

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string url,string name)
        {
            
            UIStoryboard storyboard;
            int button = await ShowAlert("Nuovo evento", name+"\n Visualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {
                    //url = "http://www.whatwhenwhere.it/?places";

                    NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                });
            }
            else { }
        }

        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }
    }
}