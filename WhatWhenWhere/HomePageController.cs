using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Threading.Tasks;
using UIKit;
using ObjCRuntime;

namespace WhatWhenWhere
{
	partial class HomePageController : UIViewController
	{

        bool backExist = false;
        bool first = true;

        bool theBool;

        NSTimer timer;

        public string LastUrl = "http://www.whatwhenwhere.it";
        public int BAckStep = 0;

        public static HomePageController Instance { private set;get;}
        public HomePageController (IntPtr handle) : base (handle)
		{
		}

        protected bool HandleStartLoad(UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
        {

            Console.WriteLine("entro qua"+ request.Url.AbsoluteString.Contains("whatsapp://"));

            if (request.Url.AbsoluteString.Contains("whatsapp://"))
                UIApplication.SharedApplication.OpenUrl(request.Url);
            //if (ShouldWebViewHandleThisUrl(request.Url)) return true;
            return true;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIApplication.SharedApplication.SetStatusBarHidden(true,false);
            UIApplication.SharedApplication.SetStatusBarHidden(false,false);

            HomePageController.Instance = this;

            AppDelegate.Instance.isHome = true;
            AppDelegate.Instance.isSetting = false;
            AppDelegate.Instance.isList = false;
            AppDelegate.Instance.isNotification = false;

            var url = "http://www.whatwhenwhere.it"; // NOTE: https secure request


            UIWebLoaderControl a = new UIWebLoaderControl(HandleStartLoad);
            WebView.ShouldStartLoad = a;

            WebView.LoadStarted += (s, e) => {

                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
                
                Console.WriteLine("Start:");
                
                ProgressView.Alpha = 1;
                Progress.Progress = 0;

                theBool = false;

                timer = NSTimer.CreateRepeatingScheduledTimer(0.1667, delegate {

                    if (theBool)
                    {
                        if (Progress.Progress >= 1)
                        {
                            ProgressView.Alpha = 0;
                            timer.Invalidate();
                        }
                        else
                        {
                            Progress.Progress += (float)0.1;
                        }
                    }
                    else
                    {
                        Progress.Progress += (float)0.05;
                        if (Progress.Progress >= 0.95)
                        {
                            Progress.Progress = (float)0.95;
                        }
                    }

                });

               LastUrl = WebView.Request.Url.ToString();
                /*if (!first)
                    WebView.StopLoading();*/

            };


            WebView.LoadFinished += (s,e) => {

                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                theBool = true;

                Console.WriteLine((WebView.Request.Url.ToString().CompareTo("http://www.whatwhenwhere.it/")) + "|" + backExist);

                if (WebView.Request.Url.ToString().CompareTo("http://www.whatwhenwhere.it/") ==0 && backExist)
                {
                    NavBarHome.Instance.RemoveBack();
                    backExist = false;
                }
                if (WebView.Request.Url.ToString().CompareTo("http://www.whatwhenwhere.it/") != 0 && !backExist)
                {
                    NavBarHome.Instance.AddBack();
                    backExist = true;
                }

                if (first)
                {
                    WebView.Alpha = 1;
                    first = false;
                }

                
                Console.WriteLine("Finish");

            };

            //WebView.load

            WebView.LoadRequest(new NSUrlRequest(new NSUrl(url),NSUrlRequestCachePolicy.ReloadIgnoringLocalCacheData,0));
            WebView.ScrollView.Bounces = false;

            reloadIcon.Clicked += delegate {

                WebView.LoadRequest(new NSUrlRequest(new NSUrl(LastUrl), NSUrlRequestCachePolicy.ReloadIgnoringLocalCacheData, 0));
                BAckStep++;

            };

            BackIcon.Clicked += delegate
            {

                Console.WriteLine("AAAA");

                /*if (backExist) { 
                    if(BAckStep != 0)
                    {
                        for(int i =0; i< BAckStep + 1; i++)
                        {
                            WebView.GoBack();
                        }
                        BAckStep = 0;
                    }
                    else*/
                        if (WebView.CanGoBack)
                            WebView.GoBack();
                //}
            };

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string url, string name)
        {

            UIStoryboard storyboard;
            int button = await ShowAlert("Nuovo evento", name + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {
                    //url = "http://www.whatwhenwhere.it/?places";

                    NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    PresentModalViewController(lp, true);
                });
            }
            else { }
        }


        public override void DismissModalViewController(bool animated)
        {
            if (this.PresentedViewController != null)
            {
                base.DismissModalViewController(animated);
            }
        }
        public override void DismissViewController(bool animated, [BlockProxy(typeof(Action))] Action completionHandler)
        {
            if (this.PresentedViewController != null)
            {
                base.DismissViewController(animated, completionHandler);
            }
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            base.WillRotate(toInterfaceOrientation, duration);
            View.SetNeedsLayout();
        }

    }
}
