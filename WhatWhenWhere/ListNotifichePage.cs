using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using RestSharp;
using System.Json;
using System.Threading.Tasks;

namespace WhatWhenWhere
{
    public partial class ListNotifichePage : UIViewController
    {

        public UITableView table;
        public int Selected;
        TableSourceNot tableSource;
        
        List<ListNot> l = new List<ListNot>();

        public UILabel text;

        float W, H, WApp, HApp;

        public bool change = false;
        public bool error = false;
        public static ListNotifichePage Instance { private set; get; }

        public ListNotifichePage (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;
            if (!change)
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            Console.WriteLine(W + "|" + H);

            UIApplication.SharedApplication.SetStatusBarHidden(true, false);
            UIApplication.SharedApplication.SetStatusBarHidden(false, false);

            ListNotifichePage.Instance = this;

            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isSetting = false;
            AppDelegate.Instance.isList = true;
            AppDelegate.Instance.isNotification = false;

            Progress.StartAnimating();
            Progress.Alpha = 1;

            /*
            l.Add(new ListNot("Not 1", "http://www.whatwhenwhere.it/event.php?e=4382"));
            l.Add(new ListNot("Not 2", "http://www.whatwhenwhere.it/event.php?e=5339"));
            l.Add(new ListNot("Not 3", "http://www.whatwhenwhere.it/event.php?e=1200"));
            l.Add(new ListNot("Not 4", "http://www.whatwhenwhere.it/event.php?e=3450"));
            l.Add(new ListNot("Not 5", "http://www.whatwhenwhere.it/event.php?e=5000"));
            */
            var size = UIStringDrawing.StringSize("Nessun evento notificato", UIFont.FromName("Lato-Regular", 19), new CGSize(W - 10, 2000));

            text = new UILabel(new CGRect(10,(H/2)-(size.Height/2),W-10,size.Height));
            text.TextColor = UIColor.White;
            text.TextAlignment = UITextAlignment.Center;
            text.Text = "Nessun evento notificato";
            text.Font = UIFont.FromName("Lato-Regular",19);
            text.BackgroundColor = UIColor.Black;
            text.Lines = 0;
            text.Alpha = 0;

            tableSource = new TableSourceNot(l);

            table = new UITableView(new CGRect(0,20,W,H-40));

            table.SeparatorColor = UIColor.Clear;
            table.Alpha = 0;
            table.BackgroundColor = UIColor.Black;
            table.Source = tableSource;

            if (!change)
                LoadData();
            else {
                if (error)
                {
                    Progress.Alpha = 0;
                    table.Alpha = 0;
                    text.Alpha = 1;
                }
                else
                {
                    Progress.Alpha = 0;
                    table.Alpha = 1;
                    text.Alpha = 0;
                }
            }

            ContentView.Add(text);
            ContentView.Add(table);
        }

        public void SelectedNot(int row)
        {
 
            NSUserDefaults.StandardUserDefaults.SetString(l[row].Url, "NotificationUrlWWW");


            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }

            UIViewController lp = storyboard.InstantiateViewController("Notification");
            //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
            PresentModalViewController(lp, true);

        }


        //**** LOAD NOTIFICATION DATA ****/
		public void LoadData()
        {

            var client = new RestClient("http://api.whatwhenwhere.it/");

            var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/notifications?os=iOS", Method.GET);

            foreach (var p in request.Parameters)
            {
                Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
            }

            client.ExecuteAsync(request, (s, e) =>
            {

                Console.WriteLine("Result ListNOT:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {
                        //string json2 =  System.IO.File.ReadAllText("JSon/test.json");

                        List<ListNot> listAppoggio = new List<ListNot>();

                        JsonValue json = JsonValue.Parse(s.Content);

                        foreach (JsonValue j in json)
                        {

                            string url = j["url"];
                            string name = j["name"];
                            string img = j["image"];

                            JsonValue jj = j["category"]; ;
                            string category = jj["name"];

                            listAppoggio.Add(new ListNot(name, url, img, category));
                        }

                        if (listAppoggio.Count == 0)
                        {
                            Progress.Alpha = 0;
                            table.Alpha = 0;
                            text.Alpha = 1;
                            error = true;
                        }
                        else
                        {
                            for (int i = listAppoggio.Count - 1; i >= 0; i--)
                            {

                                l.Add(listAppoggio[i]);
                                tableSource.element++;
                                //list.Add (listAppoggio [i]);
                                //Enumerable.Reverse (list);
                            }


                            //adapter.listChat.Add(new Message(messageT,false));
                            table.ReloadData();

                            Progress.Alpha = 0;
                            table.Alpha = 1;
                            text.Alpha = 0;
                        }
                    });

                }
                else
                {
                    Progress.Alpha = 0;
                    table.Alpha = 0;
                    text.Alpha = 1;
                    error = true;
                }
            });
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string url, string name)
        {

            UIStoryboard storyboard;
            int button = await ShowAlert("Nuovo Evento", name + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {
                    //url = "http://www.whatwhenwhere.it/?places";

                    NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    PresentModalViewController(lp, true);
                });
            }
            else { }
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            text.RemoveFromSuperview();
            //View.SetNeedsLayout();
            //ContentView.SetNeedsLayout();
            change = true;
            //SetNeedsStatusBarAppearanceUpdate();
            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

    }
}