using Foundation;
using System;
using System.Threading.Tasks;
using UIKit;

namespace WhatWhenWhere
{
    public partial class NotificationController : UIViewController
    {

        bool backExist = false;
        bool first = true;

        bool theBool;

        NSTimer timer;

        public string LastUrl ;

        public static NotificationController Instance { private set; get; }

        public NotificationController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NotificationController.Instance = this;

            
            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isSetting = false;
            AppDelegate.Instance.isList = false;
            AppDelegate.Instance.isNotification = true;

            var url = NSUserDefaults.StandardUserDefaults.StringForKey("NotificationUrlWWW"); // NOTE: https secure request
            NSUserDefaults.StandardUserDefaults.SetString("", "NotificationUrlWWW");

            LastUrl = url;

            WebView.LoadStarted += (s, e) => {

                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                Console.WriteLine("Start");

                ProgressView.Alpha = 1;
                Progress.Progress = 0;

                theBool = false;

                timer = NSTimer.CreateRepeatingScheduledTimer(0.1667, delegate {

                    if (theBool)
                    {
                        if (Progress.Progress >= 1)
                        {
                            ProgressView.Alpha = 0;
                            timer.Invalidate();
                        }
                        else
                        {
                            Progress.Progress += (float)0.1;
                        }
                    }
                    else
                    {
                        Progress.Progress += (float)0.05;
                        if (Progress.Progress >= 0.95)
                        {
                            Progress.Progress = (float)0.95;
                        }
                    }

                });

                LastUrl = url;

                /*if (!first)
                    WebView.StopLoading();*/

            };


            WebView.LoadFinished += (s, e) => {

                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                theBool = true;

                if (first)
                {
                    WebView.Alpha = 1;
                    first = false;
                }

                Console.WriteLine("Finish");

            };

            //WebView.load

            WebView.LoadRequest(new NSUrlRequest(new NSUrl(url), NSUrlRequestCachePolicy.ReloadIgnoringLocalCacheData, 0));
            WebView.ScrollView.Bounces = false;

            reloadIcon.Clicked += delegate {

                WebView.LoadRequest(new NSUrlRequest(new NSUrl(LastUrl), NSUrlRequestCachePolicy.ReloadIgnoringLocalCacheData, 0));

            };

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string url, string name)
        {

            UIStoryboard storyboard;
            int button = await ShowAlert("Nuovo Evento", name + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {
                    //url = "http://www.whatwhenwhere.it/?places";

                    NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    PresentModalViewController(lp, true);
                });
            }
            else { }
        }

    }
}