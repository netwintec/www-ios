﻿using CoreLocation;
using RestSharp;
using System;

using UIKit;
using Foundation;
using CoreGraphics;
using Carousels;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WhatWhenWhere
{
    public partial class ViewController : UIViewController
    {
        public static LocationManager Manager { get; set; }
        public bool prima;
        UIImageView pallino1, pallino2, pallino3, pallino4, pallino5;
        iCarousel carousel;
        List<string> app = new List<string>();

        float W, H2, WApp, HApp;

        public bool change = false;

        UIView Container;

        public static ViewController Instance { private set; get; }

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIApplication.SharedApplication.SetStatusBarHidden(true, false);
            UIApplication.SharedApplication.SetStatusBarHidden(false, false);

            ViewController.Instance = this;

            Manager = new LocationManager();
            Manager.StartLocationUpdates();


            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController lp = storyboard.InstantiateViewController("SWReveal");
            UIViewController np = storyboard.InstantiateViewController("Notification");

            prima = NSUserDefaults.StandardUserDefaults.BoolForKey("PrimaVoltaWWW");

            if (prima == null || prima == false)
            {

                UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;
                if (!change)
                {
                    if (orientation == UIDeviceOrientation.Portrait)
                    {
                        WApp = (float)View.Frame.Width;
                        HApp = (float)View.Frame.Height;

                        W = WApp;
                        H2 = HApp - 64;
                    }
                    else
                    {
                        HApp = (float)View.Frame.Width;
                        WApp = (float)View.Frame.Height;

                        W = HApp;
                        H2 = WApp - 64;
                    }


                }
                else
                {
                    if (orientation == UIDeviceOrientation.Portrait)
                    {
                        W = WApp;
                        H2 = HApp - 64;
                    }
                    else
                    {
                        W = HApp;
                        H2 = WApp - 64;
                    }
                }

                /* VIEW PER CAROUSEL */

                Console.WriteLine(W);

                float w, h, H;

                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    if (orientation == UIDeviceOrientation.Portrait)
                    {
                        w = (float)W - 100;
                        h = (w * 800) / 518;
                        H = h + 100;
                    }
                    else
                    {
                        h = (float)H2 - 100;
                        w = (h * 518) / 800;
                        H = h + 100;
                    }
                }
                else
                {
                    if (orientation == UIDeviceOrientation.Portrait)
                    {
                        w = (float)W - 350;
                        h = (w * 800) / 518;
                        H = h + 100;
                    }
                    else
                    {
                        h = (float)H2 - 350;
                        w = (h * 518) / 800;
                        H = h + 100;
                    }
                }

                int yy = (int)((H2 / 2) - (H / 2));

                Console.WriteLine(View.Frame.Width + "|" + View.Frame.Height + "|" + w + "|" + h + "|" + H);

                Container = new UIView(new CGRect((W / 2) - (w / 2), yy, w, h));
                Container.BackgroundColor = UIColor.Clear;

                // SETTAGGIO DELLE NOTIFICHE

                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWSport");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWTornei");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWRaduni");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWMostre");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWSfilate");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWVita");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWAperitivi");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWGay");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWOtaku");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWConcerti");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWMercatini");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWFiere");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWSagre");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWInaugurazione");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWCinema");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWTeatro");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWOnlus");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWReligione");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWBambini");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWItinerari");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWCorsi");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWPromo");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWScioperi");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "WWWPropaganda");


                if (!change) { 
                    app.Add("helpWWW1.png");
                    app.Add("helpWWW2.png");
                    app.Add("helpWWW3.png");
                    app.Add("helpWWW4.png");
                    app.Add("helpWWW5.png");
                }
                carousel = new iCarousel(new CGRect(0, 0, Container.Frame.Width, Container.Frame.Height));
                carousel.Type = iCarouselType.CoverFlow2;
                carousel.DataSource = new CarouselDataSource(5, w,h, app, 0);
                carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
                carousel.CurrentItemIndexChanged += Carousel_CurrentItemIndexChanged;

                yy += (int)h;

                pallino1 = new UIImageView(new CGRect((W / 2) - 20.5, yy + 10, 5, 5));
                pallino1.Image = UIImage.FromFile("pallinogiallo.png");

                pallino2 = new UIImageView(new CGRect((W / 2) - 11.5, yy + 10, 5, 5));
                pallino2.Image = UIImage.FromFile("pallinobianco.png");

                pallino3 = new UIImageView(new CGRect((W / 2) -2.5, yy + 10, 5, 5));
                pallino3.Image = UIImage.FromFile("pallinobianco.png");

                pallino4 = new UIImageView(new CGRect((W / 2) + 6.5, yy + 10, 5, 5));
                pallino4.Image = UIImage.FromFile("pallinobianco.png");

                pallino5 = new UIImageView(new CGRect((W / 2) + 15.5, yy + 10, 5, 5));
                pallino5.Image = UIImage.FromFile("pallinobianco.png");

                yy += 15;

                UIButton ContinuaButton = new UIButton(new CGRect(W / 2 - 125, yy+15, 250, 40));
                ContinuaButton.SetTitle("Continua", UIControlState.Normal);
                ContinuaButton.SetTitleColor(UIColor.Black, UIControlState.Normal);
                ContinuaButton.BackgroundColor = UIColor.FromRGB(255,208,4);
                ContinuaButton.Font = UIFont.FromName("Lato-Regular",16);
                ContinuaButton.TouchUpInside += delegate
                {
                    NSUserDefaults.StandardUserDefaults.SetBool(true,"PrimaVoltaWWW");
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentModalViewController(lp, true);
                };

                Container.Add(carousel);
                ContentView.Add(Container);
                ContentView.Add(pallino1);
                ContentView.Add(pallino2);
                ContentView.Add(pallino3);
                ContentView.Add(pallino4);
                ContentView.Add(pallino5);
                ContentView.Add(ContinuaButton);
            }
            else
            {
                if (AppDelegate.Instance.notification)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentModalViewController(np, true);
                }
                else
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentModalViewController(lp, true);
                }
            }

            /*ar fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }*/

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
        private void Carousel_CurrentItemIndexChanged(object sender, EventArgs e)
        {
            int index = (int)carousel.CurrentItemIndex;
            ChangePallino(index);
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            //View.SetNeedsLayout();
            //ContentView.SetNeedsLayout();

            foreach (var c in Container.Subviews)
                c.RemoveFromSuperview();

            foreach (var a in ContentView.Subviews)
                a.RemoveFromSuperview();

            change = true;
            //SetNeedsStatusBarAppearanceUpdate();
            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public void ChangePallino(int i)
        {
            if (i == 0)
            {
                pallino1.Image = UIImage.FromFile("pallinogiallo.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
                pallino5.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 1)
            {
                pallino2.Image = UIImage.FromFile("pallinogiallo.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
                pallino5.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 2)
            {
                pallino3.Image = UIImage.FromFile("pallinogiallo.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
                pallino5.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 3)
            {
                pallino4.Image = UIImage.FromFile("pallinogiallo.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino5.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 4)
            {
                pallino5.Image = UIImage.FromFile("pallinogiallo.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
            }
        }
    }

    public class CarouselDataSource : iCarouselDataSource
    {
        int[] items;
        float Widht, Height;
        List<string> listImg;
        int indice;
        public CarouselDataSource(int number, float w, float h, List<string> lI, int i)
        {
            // create our amazing data source
            items = Enumerable.Range(number, number).ToArray();
            Widht = w;
            Height = h;
            listImg = lI;
            indice = i;//(w * 850) / 695;
        }

        // let the carousel know how many items to render
        public override nint GetNumberOfItems(iCarousel carousel)
        {
            // return the number of items in the data
            return items.Length;
        }

        // create the view each item in the carousel
        public override UIView GetViewForItem(iCarousel carousel, nint index, UIView view)
        {
            UIImageView imageView = null;
            UIView View = null;

            if (view == null)
            {

                View = new UIView(new CGRect(0, 0, Widht, Height));
                View.BackgroundColor = UIColor.FromRGB(255,208,4);

                imageView = new UIImageView(new CGRect(2, 2, Widht - 4, Height - 4));
                imageView.Tag = 2;

                UIImage cachedImage = null;

                cachedImage = UIImage.FromFile(listImg[(int)index]);

                /*if (index == 0) 
                    cachedImage = UIImage.FromFile("Help1.png");

                if (index == 1)
                    cachedImage = UIImage.FromFile("Help2.png");

                if (index == 2)
                    cachedImage = UIImage.FromFile("Help3.png");

                if (index == 3)
                    cachedImage = UIImage.FromFile("Help4.png");*/

                imageView.Image = cachedImage;



                View.AddSubview(imageView);
            }
            else
            {
                // get a reference to the label in the recycled view
                View = view;
                imageView = (UIImageView)view.ViewWithTag(2);


                //label = (UILabel)view.ViewWithTag(1);
            }

            // set the values of the view

            return View;
        }
    }

    public class LocationManager
    {
        protected CLLocationManager locMgr;

        public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };
        bool first = true;
        NSDate newScan; 

        public LocationManager()
        {
            this.locMgr = new CLLocationManager();
            this.locMgr.PausesLocationUpdatesAutomatically = false;

            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                locMgr.RequestAlwaysAuthorization(); // works in background
                                                     //locMgr.RequestWhenInUseAuthorization (); // only in foreground
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                locMgr.AllowsBackgroundLocationUpdates = true;
            }

            LocationUpdated += PrintLocation;
        }

        public CLLocationManager LocMgr
        {
            get { return this.locMgr; }
        }

        public void StartLocationUpdates()
        {
            if (CLLocationManager.LocationServicesEnabled)
            {
                //set the desired accuracy, in meters
                LocMgr.DesiredAccuracy = 1000;
                LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
                {
                    // fire our custom Location Updated event
                    LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
                };
                LocMgr.StartUpdatingLocation();
            }
        }

        public void PrintLocation(object sender, LocationUpdatedEventArgs e)
        {

            CLLocation location = e.Location;
            Console.WriteLine("Altitude: " + location.Altitude + " meters");
            Console.WriteLine("Longitude: " + location.Coordinate.Longitude);
            Console.WriteLine("Latitude: " + location.Coordinate.Latitude);
            Console.WriteLine("Course: " + location.Course);
            Console.WriteLine("Speed: " + location.Speed);

            if (first)
            {
                ViewController.Instance.InvokeOnMainThread(() =>
                {

                    // Invio Dati Location
                    Console.WriteLine("Coordinate Send at http://api.whatwhenwhere.it/devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position");
                    var client = new RestClient("http://api.whatwhenwhere.it/");

                    var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position", Method.POST);
                    //request.AddHeader("Content-Type", "multipart/form-data");

                    //JObject oJsonObject = new JObject();


                    //oJsonObject.Add("os", "Android");
                    //oJsonObject.Add("latitude", location.Latitude);
                    //oJsonObject.Add("longitude", location.Longitude);
                    request.AddParameter("token", AppDelegate.Instance.APNToken);
                    request.AddParameter("os", "iOS");
                    request.AddParameter("latitude", location.Coordinate.Latitude.ToString().Replace(',','.'));
                    request.AddParameter("longitude", location.Coordinate.Longitude.ToString().Replace(',', '.'));
                    //requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                    client.ExecuteAsync(request, (s, ee) =>
                    {
                        newScan = NSDate.Now.AddSeconds(18000);
                        first = false;
                        Console.WriteLine("Result Coord:" + s.StatusCode + "|" + s.Content);

                    });

                    Console.WriteLine(1);

                });
            }
            else {
                Console.WriteLine("Prima:"+(NSDate.Now.EarlierDate(newScan) == newScan));
                if (NSDate.Now.EarlierDate(newScan) == newScan)
                {

                    ViewController.Instance.InvokeOnMainThread(() =>
                    {

                        // Invio Dati Location
                        Console.WriteLine("Coordinate Send at http://api.whatwhenwhere.it/devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position");
                        var client = new RestClient("http://api.whatwhenwhere.it/");

                        var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position", Method.POST);
                        //request.AddHeader("Content-Type", "multipart/form-data");

                        //JObject oJsonObject = new JObject();


                        //oJsonObject.Add("os", "Android");
                        //oJsonObject.Add("latitude", location.Latitude);
                        //oJsonObject.Add("longitude", location.Longitude);

                        request.AddParameter("os", "iOS");
                        request.AddParameter("latitude", location.Coordinate.Latitude.ToString().Replace(',', '.'));
                        request.AddParameter("longitude", location.Coordinate.Longitude.ToString().Replace(',', '.'));
                        //requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                        client.ExecuteAsync(request, (s, ee) =>
                        {
                            newScan = NSDate.Now.AddSeconds(18000);
                            Console.WriteLine("Result Coord:" + s.StatusCode + "|" + s.Content);

                        });

                        Console.WriteLine(1);

                    });

                }

            }
        }

        public static DateTime NSDateToDateTime(NSDate date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                new DateTime(2001, 1, 1, 0, 0, 0));
            return reference.AddSeconds(date.SecondsSinceReferenceDate);
        }



    }

    public class LocationUpdatedEventArgs : EventArgs
    {
        CLLocation location;

        public LocationUpdatedEventArgs(CLLocation location)
        {
            this.location = location;
        }

        public CLLocation Location
        {
            get { return location; }
        }
    }



}