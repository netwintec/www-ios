using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Threading.Tasks;
using UIKit;

namespace WhatWhenWhere
{
	partial class SettingPage : UIViewController
	{

        bool SportNot, TorneiNot, RaduniNot, MostreNot, SfilateNot;
        bool VitaNot, AperitiviNot, GayNot, OtakuNot, ConcertiNot;
        bool MercatiniNot, FiereNot, SagreNot, InaugurazioniNot, CinemaNot;
        bool TeatroNot, OnlusNot, ReligioneNot, BambiniNot, ItinerariNot;
        bool CorsiNot, PromoNot, ScioperiNot, PropagandaNot;

        float W, H, WApp, HApp;

        public bool change = false;

        UIScrollView scrollview;
        UIView blackView;

        public static SettingPage Instance { private set; get; }
		public SettingPage (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SettingPage.Instance = this;

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;
            if (!change)
            {
                if(orientation == UIDeviceOrientation.Portrait)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            Console.WriteLine(W + "|" + H);

            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isSetting = true;
            AppDelegate.Instance.isList = false;
            AppDelegate.Instance.isNotification = false;

            if (!change)
            {
                SportNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSport");
                TorneiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWTornei");
                RaduniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWRaduni");
                MostreNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWMostre");
                SfilateNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSfilate");
                VitaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWVita");
                AperitiviNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWAperitivi");
                GayNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWGay");
                OtakuNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWOtaku");
                ConcertiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWConcerti");
                MercatiniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWMercatini");
                FiereNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWFiere");
                SagreNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWSagre");
                InaugurazioniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWInaugurazione");
                CinemaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWCinema");
                TeatroNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWTeatro");
                OnlusNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWOnlus");
                ReligioneNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWReligione");
                BambiniNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWBambini");
                ItinerariNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWItinerari");
                CorsiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWCorsi");
                PromoNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWPromo");
                ScioperiNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWScioperi");
                PropagandaNot = NSUserDefaults.StandardUserDefaults.BoolForKey("WWWPropaganda");
            }

            scrollview = new UIScrollView(new CGRect(0, 0, W, H - 60));
            scrollview.IndicatorStyle = UIScrollViewIndicatorStyle.White;
            scrollview.ShowsVerticalScrollIndicator = true;

            int yy = 0;

            var size = UIStringDrawing.StringSize("Scegli su quale categorie rimanere aggiornato", UIFont.FromName("Lato-Regular", 27), new CGSize(W - 60, 2000));

            UILabel Label = new UILabel(new CGRect(30, yy + 15, W - 60, size.Height));
            Label.Text = "Scegli su quale categorie rimanere aggiornato";
            Label.TextColor = UIColor.White;
            Label.Font = UIFont.FromName("Lato-Regular", 27);
            Label.TextAlignment = UITextAlignment.Center;
            Label.Lines = 0;

            yy += (int)(15 + size.Height);

            UILabel SportLabel = new UILabel(new CGRect(20 , yy+15, W - 100 , 30));
            SportLabel.Text = "Sport";
            SportLabel.TextColor = UIColor.White;
            SportLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch SportSwitch = new UISwitch(new CGRect(W - 70,yy+15,50,30));
            SportSwitch.OnTintColor = UIColor.FromRGB(255,208,4);
            SportSwitch.SetState(SportNot,true);
            SportSwitch.ValueChanged += (s, e) => {

                if (SportSwitch.On)
                {
                    SportNot = true;
                }
                else
                {
                    SportNot = false;
                }

            };

            yy += 45;

            UILabel TorneiLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            TorneiLabel.Text = "Tornei";
            TorneiLabel.TextColor = UIColor.White;
            TorneiLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch TorneiSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            TorneiSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            TorneiSwitch.SetState(TorneiNot, true);
            TorneiSwitch.ValueChanged += (s, e) => {

                if (TorneiSwitch.On)
                {
                    TorneiNot = true;
                }
                else
                {
                    TorneiNot = false;
                }

            };

            yy += 45;

            UILabel RaduniLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            RaduniLabel.Text = "Raduni";
            RaduniLabel.TextColor = UIColor.White;
            RaduniLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch RaduniSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            RaduniSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            RaduniSwitch.SetState(RaduniNot, true);
            RaduniSwitch.ValueChanged += (s, e) => {

                if (RaduniSwitch.On)
                {
                    RaduniNot = true;
                }
                else
                {
                    RaduniNot = false;
                }

            };

            yy += 45;

            UILabel MostreLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            MostreLabel.Text = "Mostre";
            MostreLabel.TextColor = UIColor.White;
            MostreLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch MostreSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            MostreSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            MostreSwitch.SetState(MostreNot, true);
            MostreSwitch.ValueChanged += (s,e) => {

                if (MostreSwitch.On)
                {
                    MostreNot = true;
                }
                else
                {
                    MostreNot = false;
                }

            };

            yy += 45;

            UILabel SfilateLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            SfilateLabel.Text = "Sfilate";
            SfilateLabel.TextColor = UIColor.White;
            SfilateLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch SfilateSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            SfilateSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            SfilateSwitch.SetState(SfilateNot, true);
            SfilateSwitch.ValueChanged += (s, e) => {

                if (SfilateSwitch.On)
                {
                    SfilateNot = true;
                }
                else
                {
                    SfilateNot = false;
                }

            };

            yy += 45;

            UILabel VitaLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            VitaLabel.Text = "Vita Notturna";
            VitaLabel.TextColor = UIColor.White;
            VitaLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch VitaSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            VitaSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            VitaSwitch.SetState(VitaNot, true);
            VitaSwitch.ValueChanged += (s, e) => {

                if (VitaSwitch.On)
                {
                    VitaNot = true;
                }
                else
                {
                    VitaNot = false;
                }

            };

            yy += 45;

            UILabel AperitiviLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            AperitiviLabel.Text = "Aperitivi";
            AperitiviLabel.TextColor = UIColor.White;
            AperitiviLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch AperitiviSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            AperitiviSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            AperitiviSwitch.SetState(AperitiviNot, true);
            AperitiviSwitch.ValueChanged += (s, e) => {

                if (AperitiviSwitch.On)
                {
                    AperitiviNot = true;
                }
                else
                {
                    AperitiviNot = false;
                }

            };

            yy += 45;

            UILabel GayLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            GayLabel.Text = "Gay/LGBT";
            GayLabel.TextColor = UIColor.White;
            GayLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch GaySwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            GaySwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            GaySwitch.SetState(GayNot, true);
            GaySwitch.ValueChanged += (s, e) => {

                if (GaySwitch.On)
                {
                    GayNot = true;
                }
                else
                {
                    GayNot = false;
                }

            };

            yy += 45;

            UILabel OtakuLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            OtakuLabel.Text = "Otaku";
            OtakuLabel.TextColor = UIColor.White;
            OtakuLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch OtakuSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            OtakuSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            OtakuSwitch.SetState(OtakuNot, true);
            OtakuSwitch.ValueChanged += (s, e) => {

                if (OtakuSwitch.On)
                {
                    OtakuNot = true;
                }
                else
                {
                    OtakuNot = false;
                }

            };

            yy += 45;

            UILabel ConcertiLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            ConcertiLabel.Text = "Concerti";
            ConcertiLabel.TextColor = UIColor.White;
            ConcertiLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch ConcertiSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            ConcertiSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            ConcertiSwitch.SetState(ConcertiNot, true);
            ConcertiSwitch.ValueChanged += (s, e) => {

                if (ConcertiSwitch.On)
                {
                    ConcertiNot = true;
                }
                else
                {
                    ConcertiNot = false;
                }

            };

            yy += 45;

            UILabel MercatiniLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            MercatiniLabel.Text = "Mercatini";
            MercatiniLabel.TextColor = UIColor.White;
            MercatiniLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch MercatiniSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            MercatiniSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            MercatiniSwitch.SetState(MercatiniNot, true);
            MercatiniSwitch.ValueChanged += (s, e) => {

                if (MercatiniSwitch.On)
                {
                    MercatiniNot = true;
                }
                else
                {
                    MercatiniNot = false;
                }

            };

            yy += 45;

            UILabel FiereLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            FiereLabel.Text = "Fiere";
            FiereLabel.TextColor = UIColor.White;
            FiereLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch FiereSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            FiereSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            FiereSwitch.SetState(FiereNot, true);
            FiereSwitch.ValueChanged += (s, e) => {

                if (FiereSwitch.On)
                {
                    FiereNot = true;
                }
                else
                {
                    FiereNot = false;
                }

            };

            yy += 45;

            UILabel SagreLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            SagreLabel.Text = "Sagre";
            SagreLabel.TextColor = UIColor.White;
            SagreLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch SagreSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            SagreSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            SagreSwitch.SetState(SagreNot, true);
            SagreSwitch.ValueChanged += (s, e) => {

                if (SagreSwitch.On)
                {
                    SagreNot = true;
                }
                else
                {
                    SagreNot = false;
                }

            };

            yy += 45;

            UILabel InaugurazioniLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            InaugurazioniLabel.Text = "Inagurazioni";
            InaugurazioniLabel.TextColor = UIColor.White;
            InaugurazioniLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch InaugurazioniSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            InaugurazioniSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            InaugurazioniSwitch.SetState(InaugurazioniNot, true);
            InaugurazioniSwitch.ValueChanged += (s, e) => {

                if (InaugurazioniSwitch.On)
                {
                    InaugurazioniNot = true;
                }
                else
                {
                    InaugurazioniNot = false;
                }

            };

            yy += 45;

            UILabel CinemaLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            CinemaLabel.Text = "Cinema";
            CinemaLabel.TextColor = UIColor.White;
            CinemaLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch CinemaSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            CinemaSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            CinemaSwitch.SetState(CinemaNot, true);
            CinemaSwitch.ValueChanged += (s, e) => {

                if (CinemaSwitch.On)
                {
                    CinemaNot = true;
                }
                else
                {
                    CinemaNot = false;
                }

            };

            yy += 45;

            UILabel TeatroLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            TeatroLabel.Text = "Teatro";
            TeatroLabel.TextColor = UIColor.White;
            TeatroLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch TeatroSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            TeatroSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            TeatroSwitch.SetState(TeatroNot, true);
            TeatroSwitch.ValueChanged += (s, e) => {

                if (TeatroSwitch.On)
                {
                    TeatroNot = true;
                }
                else
                {
                    TeatroNot = false;
                }

            };

            yy += 45;

            UILabel OnlusLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            OnlusLabel.Text = "Onlus";
            OnlusLabel.TextColor = UIColor.White;
            OnlusLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch OnlusSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            OnlusSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            OnlusSwitch.SetState(OnlusNot, true);
            OnlusSwitch.ValueChanged += (s, e) => {

                if (AperitiviSwitch.On)
                {
                    OnlusNot = true;
                }
                else
                {
                    OnlusNot = false;
                }

            };

            yy += 45;

            UILabel ReligioneLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            ReligioneLabel.Text = "Religione";
            ReligioneLabel.TextColor = UIColor.White;
            ReligioneLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch ReligioneSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            ReligioneSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            ReligioneSwitch.SetState(ReligioneNot, true);
            ReligioneSwitch.ValueChanged += (s, e) => {

                if (ReligioneSwitch.On)
                {
                    ReligioneNot = true;
                }
                else
                {
                    ReligioneNot = false;
                }

            };

            yy += 45;

            UILabel BambiniLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            BambiniLabel.Text = "Bambini";
            BambiniLabel.TextColor = UIColor.White;
            BambiniLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch BambiniSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            BambiniSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            BambiniSwitch.SetState(BambiniNot, true);
            BambiniSwitch.ValueChanged += (s, e) => {

                if (BambiniSwitch.On)
                {
                    BambiniNot = true;
                }
                else
                {
                    BambiniNot = false;
                }

            };

            yy += 45;

            UILabel ItinerariLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            ItinerariLabel.Text = "Itinerari";
            ItinerariLabel.TextColor = UIColor.White;
            ItinerariLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch ItinerariSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            ItinerariSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            ItinerariSwitch.SetState(ItinerariNot, true);
            ItinerariSwitch.ValueChanged += (s, e) => {

                if (ItinerariSwitch.On)
                {
                    ItinerariNot = true;
                }
                else
                {
                    ItinerariNot = false;
                }

            };

            yy += 45;

            UILabel CorsiLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            CorsiLabel.Text = "Corsi";
            CorsiLabel.TextColor = UIColor.White;
            CorsiLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch CorsiSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            CorsiSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            CorsiSwitch.SetState(CorsiNot, true);
            CorsiSwitch.ValueChanged += (s, e) => {

                if (CorsiSwitch.On)
                {
                    CorsiNot = true;
                }
                else
                {
                    CorsiNot = false;
                }

            };

            yy += 45;

            UILabel PromoLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            PromoLabel.Text = "Promo e Sconti";
            PromoLabel.TextColor = UIColor.White;
            PromoLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch PromoSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            PromoSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            PromoSwitch.SetState(PromoNot, true);
            PromoSwitch.ValueChanged += (s, e) => {

                if (PromoSwitch.On)
                {
                    PromoNot = true;
                }
                else
                {
                    PromoNot = false;
                }

            };

            yy += 45;

            UILabel ScioperiLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            ScioperiLabel.Text = "Scioperi";
            ScioperiLabel.TextColor = UIColor.White;
            ScioperiLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch ScioperiSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            ScioperiSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            ScioperiSwitch.SetState(ScioperiNot, true);
            ScioperiSwitch.ValueChanged += (s, e) => {

                if (ScioperiSwitch.On)
                {
                    ScioperiNot = true;
                }
                else
                {
                    ScioperiNot = false;
                }

            };

            yy += 45;

            UILabel PropagandaLabel = new UILabel(new CGRect(20, yy + 15, W - 100, 30));
            PropagandaLabel.Text = "Propaganda";
            PropagandaLabel.TextColor = UIColor.White;
            PropagandaLabel.Font = UIFont.FromName("Lato-Regular", 24);

            UISwitch PropagandaSwitch = new UISwitch(new CGRect(W - 70, yy + 15, 50, 30));
            PropagandaSwitch.OnTintColor = UIColor.FromRGB(255, 208, 4);
            PropagandaSwitch.SetState(PropagandaNot, true);
            PropagandaSwitch.ValueChanged += (s, e) => {

                if (PropagandaSwitch.On)
                {
                    PropagandaNot = true;
                }
                else
                {
                    PropagandaNot = false;
                }

            };

            yy += 45;

            scrollview.ContentSize = new CGSize(W,yy+20);

            scrollview.Add(Label);
            scrollview.Add(SportLabel);
            scrollview.Add(SportSwitch);
            scrollview.Add(TorneiLabel);
            scrollview.Add(TorneiSwitch);
            scrollview.Add(RaduniLabel);
            scrollview.Add(RaduniSwitch);
            scrollview.Add(MostreLabel);
            scrollview.Add(MostreSwitch);
            scrollview.Add(SfilateLabel);
            scrollview.Add(SfilateSwitch);
            scrollview.Add(VitaLabel);
            scrollview.Add(VitaSwitch);
            scrollview.Add(AperitiviLabel);
            scrollview.Add(AperitiviSwitch);
            scrollview.Add(GayLabel);
            scrollview.Add(GaySwitch);
            scrollview.Add(OtakuLabel);
            scrollview.Add(OtakuSwitch);
            scrollview.Add(ConcertiLabel);
            scrollview.Add(ConcertiSwitch);
            scrollview.Add(MercatiniLabel);
            scrollview.Add(MercatiniSwitch);
            scrollview.Add(FiereLabel);
            scrollview.Add(FiereSwitch);
            scrollview.Add(SagreLabel);
            scrollview.Add(SagreSwitch);
            scrollview.Add(InaugurazioniLabel);
            scrollview.Add(InaugurazioniSwitch);
            scrollview.Add(CinemaLabel);
            scrollview.Add(CinemaSwitch);
            scrollview.Add(TeatroLabel);
            scrollview.Add(TeatroSwitch);
            scrollview.Add(OnlusLabel);
            scrollview.Add(OnlusSwitch);
            scrollview.Add(ReligioneLabel);
            scrollview.Add(ReligioneSwitch);
            scrollview.Add(BambiniLabel);
            scrollview.Add(BambiniSwitch);
            scrollview.Add(ItinerariLabel);
            scrollview.Add(ItinerariSwitch);
            scrollview.Add(CorsiLabel);
            scrollview.Add(CorsiSwitch);
            scrollview.Add(PromoLabel);
            scrollview.Add(PromoSwitch);
            scrollview.Add(ScioperiLabel);
            scrollview.Add(ScioperiSwitch);
            scrollview.Add(PropagandaLabel);
            scrollview.Add(PropagandaSwitch);

            UIImageView alpha = new UIImageView(new CGRect(0, H - 80,W,20));
            alpha.Image = UIImage.FromFile("alphaSetting.png");

            blackView = new UIView(new CGRect(0, H - 60, W, 60));
            blackView.BackgroundColor = UIColor.Black;

            UIButton SaveButton = new UIButton(new CGRect(W / 2 - 125, 10, 250, 40));
            SaveButton.SetTitle("Salva", UIControlState.Normal);
            SaveButton.SetTitleColor(UIColor.Black, UIControlState.Normal);
            SaveButton.BackgroundColor = UIColor.FromRGB(255, 208, 4);
            SaveButton.Font = UIFont.FromName("Lato-Regular", 22);
            SaveButton.TouchUpInside += delegate
            {

                NSUserDefaults.StandardUserDefaults.SetBool(SportNot,"WWWSport");
                NSUserDefaults.StandardUserDefaults.SetBool(TorneiNot,"WWWTornei");
                NSUserDefaults.StandardUserDefaults.SetBool(RaduniNot,"WWWRaduni");
                NSUserDefaults.StandardUserDefaults.SetBool(MostreNot,"WWWMostre");
                NSUserDefaults.StandardUserDefaults.SetBool(SfilateNot,"WWWSfilate");
                NSUserDefaults.StandardUserDefaults.SetBool(VitaNot,"WWWVita");
                NSUserDefaults.StandardUserDefaults.SetBool(AperitiviNot,"WWWAperitivi");
                NSUserDefaults.StandardUserDefaults.SetBool(GayNot, "WWWGay");
                NSUserDefaults.StandardUserDefaults.SetBool(OtakuNot,"WWWOtaku");
                NSUserDefaults.StandardUserDefaults.SetBool(ConcertiNot,"WWWConcerti");
                NSUserDefaults.StandardUserDefaults.SetBool(MercatiniNot,"WWWMercatini");
                NSUserDefaults.StandardUserDefaults.SetBool(FiereNot,"WWWFiere");
                NSUserDefaults.StandardUserDefaults.SetBool(SagreNot,"WWWSagre");
                NSUserDefaults.StandardUserDefaults.SetBool(InaugurazioniNot,"WWWInaugurazione");
                NSUserDefaults.StandardUserDefaults.SetBool(CinemaNot,"WWWCinema");
                NSUserDefaults.StandardUserDefaults.SetBool(TeatroNot,"WWWTeatro");
                NSUserDefaults.StandardUserDefaults.SetBool(OnlusNot,"WWWOnlus");
                NSUserDefaults.StandardUserDefaults.SetBool(ReligioneNot,"WWWReligione");
                NSUserDefaults.StandardUserDefaults.SetBool(BambiniNot,"WWWBambini");
                NSUserDefaults.StandardUserDefaults.SetBool(ItinerariNot,"WWWItinerari");
                NSUserDefaults.StandardUserDefaults.SetBool(CorsiNot,"WWWCorsi");
                NSUserDefaults.StandardUserDefaults.SetBool(PromoNot,"WWWPromo");
                NSUserDefaults.StandardUserDefaults.SetBool(ScioperiNot,"WWWScioperi");
                NSUserDefaults.StandardUserDefaults.SetBool(PropagandaNot,"WWWPropaganda");

                new UIAlertView("Impostazioni","Salvataggio Riuscito",null,"OK",null).Show();


                Console.WriteLine("Notification Send at http://api.whatwhenwhere.it/devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/position");
                var client = new RestClient("http://api.whatwhenwhere.it/");

                var request = new RestRequest("devices/" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString() + "/categories", Method.POST);
                //request.AddHeader("Content-Type", "multipart/form-data");

                //JObject oJsonObject = new JObject();


                //oJsonObject.Add("os", "Android");
                //oJsonObject.Add("latitude", location.Latitude);
                //oJsonObject.Add("longitude", location.Longitude);
                request.AddParameter("os", "iOS");
                request.AddParameter("token", AppDelegate.Instance.APNToken);

                if (SportNot)
                    request.AddParameter("categories[]", "sport");
                if (TorneiNot)
                    request.AddParameter("categories[]", "tournaments");
                if (RaduniNot)
                    request.AddParameter("categories[]", "gatherings");
                if (MostreNot)
                    request.AddParameter("categories[]", "exhibitions");
                if (SfilateNot)
                    request.AddParameter("categories[]", "parades");
                if (VitaNot)
                    request.AddParameter("categories[]", "nightlife");
                if (AperitiviNot)
                    request.AddParameter("categories[]", "aperitives");
                if (GayNot)
                    request.AddParameter("categories[]", "gay-lgbt");
                if (OtakuNot)
                    request.AddParameter("categories[]", "otaku");
                if (ConcertiNot)
                    request.AddParameter("categories[]", "concerts");
                if (MercatiniNot)
                    request.AddParameter("categories[]", "markets");
                if (FiereNot)
                    request.AddParameter("categories[]", "fairs");
                if (SagreNot)
                    request.AddParameter("categories[]", "festivals");
                if (InaugurazioniNot)
                    request.AddParameter("categories[]", "inaugurations");
                if (CinemaNot)
                    request.AddParameter("categories[]", "cinema");
                if (TeatroNot)
                    request.AddParameter("categories[]", "theatre");
                if (OnlusNot)
                    request.AddParameter("categories[]", "onlus");
                if (ReligioneNot)
                    request.AddParameter("categories[]", "religion");
                if (BambiniNot)
                    request.AddParameter("categories[]", "kids");
                if (ItinerariNot)
                    request.AddParameter("categories[]", "routes");
                if (CorsiNot)
                    request.AddParameter("categories[]", "training-courses");
                if (PromoNot)
                    request.AddParameter("categories[]", "promos-discounts");
                if (ScioperiNot)
                    request.AddParameter("categories[]", "strike");
                if (PropagandaNot)
                    request.AddParameter("categories[]", "propaganda");

                //requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                foreach (var p in request.Parameters)
                {
                    Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
                }

                client.ExecuteAsync(request, (s, e) =>
                {

                    Console.WriteLine("Result Not:" + s.StatusCode + "|" + s.Content);

                });

                Console.WriteLine(1);

            };

            blackView.Add(SaveButton);

            ContentView.Add(scrollview);
            ContentView.Add(alpha);
            ContentView.Add(blackView);


        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string url, string name)
        {

            UIStoryboard storyboard;
            int button = await ShowAlert("Nuovo Evento", name + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {
                    //url = "http://www.whatwhenwhere.it/?places";

                    NSUserDefaults.StandardUserDefaults.SetString(url, "NotificationUrlWWW");

                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    PresentModalViewController(lp, true);
                });
            }
            else { }
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in scrollview.Subviews)
                b.RemoveFromSuperview();

            foreach (var c in blackView.Subviews)
                c.RemoveFromSuperview();

            foreach (var a in ContentView.Subviews)
                a.RemoveFromSuperview();

            change = true;
            //View.SetNeedsLayout();
            //ContentView.SetNeedsLayout();
            //SetNeedsStatusBarAppearanceUpdate();
            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }



    }
}
